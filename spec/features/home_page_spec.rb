require 'spec_helper'

describe 'Home Page' do

  it "shold have text Learning Layouts" do
    visit "/"
    page.should have_content "Learning Layouts"
  end

  it "should have title Learning Layouts" do
    visit "/"
    page.should have_title "Learning Layouts"
  end

  # This test is failing. Need to fix routing and href option in have_link
  it "should have link 'Display Property'" do
    visit "/"
    page.should have_link("Display Property", :href => "/DisplayProperty")

  end

end

