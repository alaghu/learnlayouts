require 'spec_helper'

describe 'Display Page' do

  it "should navigate from Home page" do
    visit "/"
    click_link "Display Property"

  end

  it "should navigate back to the Home page" do
    visit "/DisplayProperty"
    click_link "Home"
  end



  it "should have text about Block and inline" do
    visit "/DisplayProperty"
    page.should have_content "This is about the Display Property."
    page.should have_content "Display:Block or Display:inline"
    page.should have_content "Div is a block. So is p and form."
    page.should have_content "A block starts a new line and stretches out as
far as it can."
    page.should have_content "Span and a are inline. Notice that it wraps
inside the paragraph by not disrupting the flow of the para."
  end

  it "should have about none and visibility property" do
    visit "/DisplayProperty"
    page.should have_title "Display:None vs Visibility:None"
  end

  # This test is failing. Need to fix routing and href option in have_link
  it "should have link 'Display Property'" do
    visit "/"
    page.should have_link("Display Property", :href => "/DisplayProperty")
  end

end

